import ReactDOM from 'react-dom'; // Se importa para poder trabajar en el dom
import './index.css'; // De esta manera se puede importar un css

// ESTO ES UTILIZANDO LAS PROPS

// Todos los componentes comienzan en Mayuscula
const Title = ({ texto }) => <h3>{texto}</h3>;
// Esto es un componente donde por medio de una funcion de flecha devuelvo una etiqueta.
// al momento de recibir los datos puedo indicar que datos son los que quiero usar.

// De esta forma se pasas las propiedades de los elementos a las funciones
const app = (
	<div>
		<Title texto='hola' />
		<Title texto='Qlq' />
		<Title texto='Habla' />
	</div>
);

// De esta forma se usan los children(Hijos)
const Spam = (props) => <spam> {props.children} </spam>;

// De esta manera se pasan los childrens a las funciones
const prueba = (
	<div>
		<Spam> Prueba desde Spam</Spam>
	</div>
);

const container = document.getElementById('root');

ReactDOM.render([app, prueba], container); // se renderiza un elemento en el contenedor indicado

/* const app = (
	<h1 className='title' onClick={() => alert('hola')}>
		Cliqueame
	</h1>
); */

// Esto es con jsx Un lenguaje de plantilla. Cada etiqueta posee propiedades las cuales se manejan
// con camelCase, En caso de ser eventos de js se necesita pasar una funcion
// className permite agregar clases a los elementos.
