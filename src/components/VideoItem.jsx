import PropTypes from 'prop-types'; // Para realizar las validaciones de los Props
import style from './VideoItem.module.css'; // De esta manera se trabaja con css module

const VideoItem = ({ title, description, author }) => {
	// De esta manera se puede pasar 2 clases mismo elemento
	return (
		<div className={`${style.wrapper} ${style.margin}`}>
			<h3>{title}</h3>
			<div>
				<span>Created at: {author}</span>
			</div>
			<p>Infomation: {description}</p>
		</div>
	);
};

// De esta manera se realizan las validaciones a los props.
// Al componente se le indica los tipos de props
VideoItem.propTypes = {
	title: PropTypes.string.isRequired, // Llamando a la libreria que fue importada y se le indica el tipo de propiedad y si es requerida
	description: PropTypes.string,
	author: PropTypes.string.isRequired,
};

VideoItem.defaultProps = {
	description: 'Sin descripcion',
};

export default VideoItem;
