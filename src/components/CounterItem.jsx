import { useState, useEffect } from 'react'; // para cambiar el estado de un elemento (Valor)
import PropTypes from 'prop-types';
import style from '../App.module.css';

const CounterItem = ({ value }) => {
	/*
		Al useState se le pasa el valor inicial. Este devuelve un array, donde el
		primer valor es el valor actual de elemeto
		Y el segundo es una funcion la cual se encarga de cambiar este valor
	*/
	const [counter, setCounter] = useState(value);

	/*
	El useEffect se usa para realizar todas las acciones necesaria luego de un cambio de estado
	Se le pasa una funcion en donde se le indican todas las acciones a ejecutar.
	Es importante definir cuales son las acciones a ejecutar y cual es la funcionalidad principal
	*/
	useEffect(() => {
		if (counter !== 0) document.title = counter;
	});

	const changeValueAddition = () => {
		// De esta manera se cambia el estado del elemento. Se Usa la funcion que nos devuelve el useState se le envia por parametro el nuevo valor
		setCounter(counter + 1);
		// Esta es otra forma en donde se para por parametro una funcion flecha en la cual se le retorna la operacion.
		// setCounter( ( c ) => c +1 );
	};

	const changeValueSubtraction = () => setCounter(counter - 1);

	const resetValue = () => setCounter(value);

	return (
		<div className={style.container}>
			<h1>Counter App</h1>
			<h2> {counter} </h2>
			<button onClick={changeValueAddition}> +1 </button>
			<button onClick={changeValueSubtraction}> --1 </button>
			<button onClick={resetValue}> Reset </button>
		</div>
	);
};

CounterItem.propTypes = {
	value: PropTypes.number.isRequired,
};

CounterItem.defaultProps = {
	value: 0,
};

export default CounterItem;
