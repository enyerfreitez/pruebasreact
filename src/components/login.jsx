import { useEffect, useState } from 'react';

/* 
    Form donde se toman los datos en tiempo real,
    con el onChange puedo ir tomando el dato apenas cambia y
    Uso los useState para poder mostrar y cambiar los datos al momento.
    Por ende puedo ir validando segun el usario escribe.
    onChange recibe una funcion flecha en la cual tomo el valor del input que se esta cambiando
    y con el value puedo modificar el input
*/

const Login = () => {
	const [password, setPassword] = useState('');
	const [email, setEmail] = useState('');

	useEffect(() => {
		if (password === '') return;
		document.title = 'Modificando';
	}, [password]);
	// Al useEffect se le puede indicar cuando se ejecutara pasandole por un array
	// un estado o un prop los cuales son los que pueden cambiar. Mayormente es el state

	const errorMessage = validateData(email, password);

	return (
		<form
			onSubmit={(ev) => {
				ev.preventDefault();
				alert(login(email, password));
			}}
		>
			<input
				type='text'
				name='email'
				autoComplete='off'
				value={email}
				onChange={(ev) => setEmail(ev.target.value)}
			/>
			<input
				type='password'
				name='password'
				value={password}
				onChange={(ev) => setPassword(ev.target.value)}
			/>
			<p>{errorMessage}</p>
			<button type='submit' disabled={errorMessage}>
				Enviar
			</button>
			<button
				type='button'
				onClick={() => {
					setEmail('');
					setPassword('');
					document.title = 'App Vite';
				}}
			>
				Clear
			</button>
		</form>
	);
};

const login = (email, password) => {
	if (email !== 'enyerfreitez@gmail.com' || password !== 'enyer')
		return 'data incorrect';
	return 'login correct';
};

const validateData = (email, password) => {
	if (!email.includes('@')) return 'Format email incorrect';
	if (password.length < 4) return 'Format password incorrect';
};

export default Login;
