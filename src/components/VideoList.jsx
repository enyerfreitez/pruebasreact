import style from './VideoList.module.css';
import PropTypes from 'prop-types';

const VideoList = ({ title, children }) => {
	// Con en className el css importado como objeto se accede a la clase en cuestion
	return (
		<div className={style.wrapper}>
			<h2>{title}</h2>
			{children || 'No hay videos'}
		</div>
	);
};

VideoList.propTypes = {
	title: PropTypes.string.isRequired,
};

export default VideoList;
