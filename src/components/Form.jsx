import { useState, useEffect, useRef, useContext } from 'react';

/** Hooks:
 *
 * << useState >> Permite modificar y acceder a el valor de un elemento que se encuentre implicado en la vista.
 * 	Gracias a el react sabe que renderizar solo cuando el estado del elemento cambia.
 *
 * << useEffect >> Permite efecutar fragmento de codigo apenas el stado de un elemento cambie, Este recibe dos parametros
 * 	la primera es un callback en el cual se envia el codigo a ejecutar, el segundo es un array indicando el estado que ejecute
 *  la funcion al cambiar su valor. Si el array esta vacio [] se ejecutara solo la primera vez que se cambie un estado
 *
 * <<useRef>> Permite indicarle a react que se usara un elemento en cuestion para manipular el DOM directamente
 *  Para usarlo se crea una variable la cual guarda la referencia y luego usando el atributo ref en el elemento en cuestion
 *  se le indica al hook que el sera la referencia. Para hacer uso de la refencia creada se hace de la siguiente manera
 *  nombreReferencia.current.FuncionAUSar.
 *
 * <<useContext>> Permite acceder al contenido creado por el elemento padre de componente en cuestion. Se crea
 *  una variable la cual contendra con contexto este es un obj por ende se puede acceder a sus atributos por medio de puntos
 *
 */

import { ThemeContext } from '../ShowForm'; // Este el el padre que contiene el contexto a usar

const Form = ({ shower }) => {
	const [title, setTitle] = useState('');
	const [body, setBody] = useState('');

	useEffect(() => {
		if (shower) {
			refInput.current.focus();
		}
		return () => {
			// Esta funcion se ejecuta cada vez que el elemento se elimina de las vista
			console.log('QLQ');
		};
	}, [shower]);

	const refInput = useRef();

	const context = useContext(ThemeContext);

	const sendForm = (ev) => {
		ev.preventDefault();

		// De esta manera se puede acceder a una API desde react
		fetch('https://jsonplaceholder.typicode.com/posts', {
			method: 'POST',
			body: JSON.stringify({
				title,
				body,
				userId: 1,
			}),
			headers: {
				'Content-type': 'application/json; charset=UTF-8',
			},
		})
			.then((response) => response.json())
			.then((json) => {
				setTitle('');
				setBody('');
				console.log(json);
			});
	};

	return (
		<form onSubmit={(ev) => sendForm(ev)}>
			<p>Form</p>
			<div>
				<label htmlFor='title'>Title</label>
				<input
					type='text'
					id='title'
					value={title}
					onChange={(ev) => setTitle(ev.target.value)}
					ref={refInput}
				/>
			</div>
			<div>
				<label htmlFor='body'>Body</label>
				<textarea
					id='body'
					cols='30'
					rows='10'
					value={body}
					onChange={(ev) => setBody(ev.target.value)}
				></textarea>
			</div>
			<input
				type='submit'
				value='Enviar'
				style={{
					background: context.background,
					color: context.color,
				}}
			/>
		</form>
	);
};

export default Form;
