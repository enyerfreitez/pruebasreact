import VideoList from './components/VideoList';
import VideoItem from './components/VideoItem';
import style from './App.module.css';

const App = () => (
	<div className={style.container}>
		<VideoList title='Curso Node'>
			<VideoItem
				title='Primera Clase'
				description='Informacion de la primera clase node'
				author='enyer95'
			></VideoItem>
			<VideoItem
				title='Segunda Clase'
				description='Informacion de la segunda clase'
				author='enyer95'
			></VideoItem>
		</VideoList>
		<VideoList title='Curso React'>
			<VideoItem title='Primera Clase' author='jesusjclark'></VideoItem>
		</VideoList>
		<VideoList title='Futura clases'></VideoList>
	</div>
);

export default App;
