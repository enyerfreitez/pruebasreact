import { createRoot } from 'react-dom/client';
// import CounterItem from './components/CounterItem';
// import App from './App';
// import Login from './components/login';
import { ShowForm } from './ShowForm';

const container = document.getElementById('root');

createRoot(container).render(<ShowForm />);
