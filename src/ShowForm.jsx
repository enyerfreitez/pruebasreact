import React, { useState, Suspense } from 'react';

/**
 * Esto es un import dinamico el cual no importa el componente hasta que sea necesario usarlo (Se puede usar en modales, tables y demas )
 * La funcion de la class React la cula recibe una funcion de flecha donde se indica que se importara. Esto devuelve una promesa
 */
const Form = React.lazy(() => import('./components/Form'));

const themes = {
	dark: {
		background: '#000',
		color: '#FFF',
	},
	light: {
		background: '#FFF',
		color: '#000',
	},
};

/**
 * 	Cuando sea necesario pasar informacion entre varios componentes como puede ser, el tema a usar, el nombre del usuario, si esta logueado y mas
 * se puede vandar entre componentes como Prop's pero esto no muy viable, porque puede ser muy grande la cantidad de hijos a los cuales se le tendria que pasar.
 * Y por ende es propenso a errores.
 * 	Por ende recomendable usar la funcion de react createContext() La cual se puede decir que hace el papel de provedor para todos sus hijos,
 * permitiendo asi que estos accedan a su informacion sin la necesaridad de enviarla como Prop's.
 * 	Pero esta se necesita exportar para que sus hijos puedan acceder a ella.
 */
export const ThemeContext = React.createContext();

export const ShowForm = () => {
	const [show, setShow] = useState(false);
	const [theme, setTheme] = useState(themes.light);

	/**
	 * <<Suspense>> Se usa el suspense como un componente donde se le envia la Prop (fallback) En la cual se le indica que mostrar
	 * 		mientras que se realiza la importacion, puede ser un mensaje o una animacion de carga.
	 * 	 Como Hijo se le envia El componente a importar. Esto es porque al se una promesa react no puede esperar a que este listo
	 * 		y quedarse sin mostrar nada. Por ende se le envia en fallback para que muestre mientras.
	 *
	 * <<Elemento.Provider>> Se usa como un componente donde se le pasa como Prop (value) el valor que sera usado en sus hijos.
	 * 	 Dentro de este se colocara todos los hijos que necesiten acceder a dicha informacion.
	 */
	return (
		<div>
			<ThemeContext.Provider value={theme}>
				{show && (
					<Suspense fallback={<p>Cargando...</p>}>
						<Form shower={show} />
						<div>
							<label htmlFor='radioThemeDark'>Cambiar tema Oscuro</label>
							<input
								type='radio'
								name='radioTheme'
								id='radioThemeDark'
								onChange={(ev) => {
									setTheme(themes.dark);
								}}
							/>
							<label htmlFor='radioThemeLight'>Cambiar tema claro</label>
							<input
								type='radio'
								name='radioTheme'
								id='radioThemeLight'
								onChange={(ev) => {
									setTheme(themes.light);
								}}
							/>
						</div>
					</Suspense>
				)}
			</ThemeContext.Provider>
			<button onClick={() => setShow(!show)}>
				{show ? 'Ocultar' : 'Mostrar'} Formulario
			</button>
		</div>
	);
};
